import React, { Component } from 'react';
import './App.css';
import Maincomp from './components/main.js';
import 'bootstrap/dist/css/bootstrap.min.css';

import {BrowserRouter} from 'react-router-dom';



class App extends Component {
  render(){
  return (
   
    <BrowserRouter>
    <div className="App">
      
        <Maincomp />
      
    </div>
    </BrowserRouter>
  );}
}

export default App;
