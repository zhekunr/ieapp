import React from 'react';
import { Jumbotron } from 'reactstrap';
import Form from 'react-bootstrap/Form';
import { Container, Button } from 'reactstrap';

function Information(props) {
  return (
    <div>
      <Jumbotron>
        <div className="container">
          <div className="row row-header">
            <div className="col-12 col-sm-10">
              <h1>Allergy Replacement</h1>
              <p>Allergy to peanut? Don't worry, we will find a replacement of any allergitic food for you! </p>
            </div>
          </div>
        </div>
      </Jumbotron>



      <div className="row-content">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-4">
              <Form >
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label><h><b>Food you are allergic to:</b></h></Form.Label>
                  <Form.Control type="foodinput" placeholder="peanut" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Label><h><b>Food Type</b></h></Form.Label>
                  <Form.Control as="select">
                    <option>Vegetables</option>
                    <option>Fruit</option>
                    <option>Meat</option>
                    <option>Others</option>
                  </Form.Control>
                </Form.Group>
              </Form>
            </div>
            <div className="col-12 col-md-2 offset-md-1">
              <br></br>
              <br></br>
              <br></br>
              <Button >Find</Button>
            </div>
            <div className="col-12 col-md-4">
            <br></br>
              <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label> <h><b>The replacements are:</b></h></Form.Label>
                <Form.Control as="textarea" rows="3" />
              </Form.Group>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Information;   