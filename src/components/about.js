import React from 'react';
import { Jumbotron } from 'reactstrap';
import Card from 'react-bootstrap/Card'
import { Container, Button } from 'reactstrap'

function About(props) {
  return (

    <div>
      <Jumbotron>
        <div className="container">
          <div className="row row-header">
            <div className="col-12 col-sm-10">
              <h1>Health and Disease </h1>
              <p>Want to know about the relation between health and disease? have a look here! </p>
            </div>
          </div>
        </div>
      </Jumbotron>
      <div className="row-content">
        <div className="container">
        <div className="row justify-content-center" id="homecards">
            <div className="col-10 col-md-3">
              <Card >
                <Card.Img variant="top" height="150px" src={require("./assets/challenge.png")} />
                <Card.Body>
                  <Card.Title>News1</Card.Title>

                  <Card.Text>description of news</Card.Text>
                </Card.Body>
              </Card>
            </div>
            <div className="col-10 col-md-3">
              <Card>
                <Card.Img variant="top" height="150px" src={require("./assets/peanut.jpg")} />
                <Card.Body>
                  <Card.Title>News2</Card.Title>

                  <Card.Text>description of news</Card.Text>
                </Card.Body>
              </Card>
            </div>
            <div className="col-10 col-md-3">
              <Card>
                <Card.Img variant="top" height="150px" src={require("./assets/health.jpg")} />
                <Card.Body>
                  <Card.Title>News3</Card.Title>

                  <Card.Text>description of news</Card.Text>
                </Card.Body>
              </Card>
            </div>
          </div>
          <div className="row justify-content-center" id="homecards">
            <div className="col-10 col-md-3">
              <Card >
                <Card.Img variant="top" height="150px" src={require("./assets/challenge.png")} />
                <Card.Body>
                  <Card.Title>News4</Card.Title>

                  <Card.Text>description of news</Card.Text>
                </Card.Body>
              </Card>
            </div>
            <div className="col-10 col-md-3">
              <Card>
                <Card.Img variant="top" height="150px" src={require("./assets/peanut.jpg")} />
                <Card.Body>
                  <Card.Title>News5</Card.Title>

                  <Card.Text>description of news</Card.Text>
                </Card.Body>
              </Card>
            </div>
            <div className="col-10 col-md-3">
              <Card>
                <Card.Img variant="top" height="150px" src={require("./assets/health.jpg")} />
                <Card.Body>
                  <Card.Title>News6</Card.Title>

                  <Card.Text>description of news</Card.Text>
                </Card.Body>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </div>



  );
}

export default About;   