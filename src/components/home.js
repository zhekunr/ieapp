import React from 'react';
import Carousel from 'react-bootstrap/Carousel'
import Card from 'react-bootstrap/Card'
import { Container, Button } from 'reactstrap'
import { useHistory } from "react-router-dom"
import { Jumbotron } from 'reactstrap';




function HomeCarousel() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className=" w-100"
          src={require("./assets/challenge.png")}
          alt="First slide"
          height='200px'
        />
        <Carousel.Caption>
          <h3>Daily Challenge</h3>
          <p>Want to kick start your healthy lifestyle? Start exercise and eating healthy TODAY!</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="w-100 "
          src={require("./assets/peanut.jpg")}
          alt="Third slide"
          height='200px'
        />

        <Carousel.Caption>
          <h3>Allergy Relacement</h3>
          <p>You have an allergy? Don't worried, we will find the replacement food just for you</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="w-100 "
          src={require("./assets/health.jpg")}
          alt="Third slide"
          height='200px'
        />

        <Carousel.Caption>
          <h3>Heath & Disease</h3>
          <p>Here you can find all the information about how to get rid of nutrition deficiency.</p>
        </Carousel.Caption>
      </Carousel.Item>


    </Carousel>
  );
}

function Home(props) {
  let history = useHistory();

  function gotoChallenge() {
    history.push("/diagnose");
  }
  function gotoAllergy() {
    history.push("/information");
  }
  function gotoAbout() {
    history.push("/about")
  }
  return (
    <div>
      <Jumbotron>
        <div className="container">
          <div className="row row-header">
            <div className="col-12 col-sm-10">
              <h1>Welcome to LYZY Health </h1>
              <p>Hi! We are LYZY, guess you are as lazy as we are? Join us to change your lifestyle from today, say goodby to lazy, say hi to LYZY!</p>
            </div>
          </div>
        </div>
      </Jumbotron>
      <div className="row-content">

        <div className="container">

          <div className="row justify-content-center">
            <div className="col-10 ">
              <HomeCarousel />
            </div>
          </div>
          <div className="row justify-content-center" id="homecards">
            <div className="col-10 col-md-3">
              <Card >
                <Card.Img variant="top" height="150px" src={require("./assets/challenge.png")} />
                <Card.Body>
                  <Card.Title>Daily Challenge</Card.Title>

                  <Button variant="primary" onClick={gotoChallenge}>Go Challenge</Button>
                </Card.Body>
              </Card>
            </div>
            <div className="col-10 col-md-3">
              <Card>
                <Card.Img variant="top" height="150px" src={require("./assets/peanut.jpg")} />
                <Card.Body>
                  <Card.Title>Allergy Replacement</Card.Title>

                  <Button variant="primary" onClick={gotoAllergy}>Go Allergy</Button>
                </Card.Body>
              </Card>
            </div>
            <div className="col-10 col-md-3">
              <Card>
                <Card.Img variant="top" height="150px" src={require("./assets/health.jpg")} />
                <Card.Body>
                  <Card.Title>Health and Disease</Card.Title>

                  <Button variant="primary" onClick={gotoAbout}>Go About</Button>
                </Card.Body>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;   