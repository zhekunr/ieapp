import React from 'react';
import { Jumbotron } from 'reactstrap';
import Card from 'react-bootstrap/Card';
import { Container, Button } from 'reactstrap';
import ListGroup from 'react-bootstrap/ListGroup'

function Diagnose(props) {
  return (
    <div>
      <Jumbotron>
        <div className="container">
          <div className="row row-header">
            <div className="col-12 col-sm-10">
              <h1>Daily Challenge</h1>
              <p>Yo! Can you finish the daily challenge here? Come and see how far can you go!</p>
            </div>
          </div>
        </div>
      </Jumbotron>
      <div className="row-content">
        <div className="container">
          <div className="row">
          <div className="col-12 col-md-8">
            <Card >
              <Card.Img variant="top" src={require("./assets/challenge.png")} />
              <Card.Body>
                <Card.Title>First Challenge</Card.Title>
                <Card.Text>You can only walk or run to school today!</Card.Text>
                <Button variant="primary" >Change a Challenge</Button>
                <Button variant="primary" className="ml-2">Done</Button>
              </Card.Body>
            </Card>
          </div>
          <div className="col-12 col-md-3 offset-md-1">
            <ListGroup>
              <ListGroup.Item disabled><b>Ranking Board</b></ListGroup.Item>
              <ListGroup.Item>1. Dapibus ac facilisis in</ListGroup.Item>
              <ListGroup.Item>2. Morbi leo risus</ListGroup.Item>
              <ListGroup.Item>3. Porta ac consectetur ac</ListGroup.Item>
              <ListGroup.Item>4. Vestibulum at eros</ListGroup.Item>
              <ListGroup.Item>5. Dapibus ac facilisis in</ListGroup.Item>
              <ListGroup.Item>6. Morbi leo risus</ListGroup.Item>
              <ListGroup.Item>7. Porta ac consectetur ac</ListGroup.Item>
              <ListGroup.Item>8. Vestibulum at eros</ListGroup.Item>
              <ListGroup.Item>9. Porta ac consectetur ac</ListGroup.Item>
              <ListGroup.Item>10. Vestibulum at eros</ListGroup.Item>
            </ListGroup>
          </div>
          </div>
         
        </div>
      </div>
    </div>
  );
}

export default Diagnose;   