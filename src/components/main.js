

import React,{Component} from 'react';
import Header from './header.js';
import Footer from './footer.js';
import Home from './home.js';
import Diagnose from './diagnose.js';
import Doctors from './doctors.js';
import Information from './information.js';
import About from './about.js';

import { Switch, Route, Redirect,withRouter } from 'react-router-dom';



class Maincomp extends Component{


    render(){
        const HomePage=() =>{
            return(
                <Home/>
            )
        }

        return(
            <div>
                <Header />
               

                    <Switch>
                        <Route path='/home' component={HomePage}/>
                        <Route exact path='/diagnose' component={() => <Diagnose />} />
                        <Route exact path='/doctors' component={() => <Doctors />} />
                        <Route exact path='/information' component={() => <Information />} />
                        <Route exact path='/about' component={() => <About />} />
                        <Redirect to="/home"/>
                    </Switch>
                <Footer />
            </div>
        );
    }

}

export default withRouter(Maincomp);